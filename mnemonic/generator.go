package mnemonic

import (
	"context"
	"errors"
	"fmt"
	"strings"

	"github.com/brianium/mnemonic"
	"gitlab.com/chanshare/provisioner/util"
	"go.opentelemetry.io/otel"
	"go.uber.org/zap"
)

const Mnemonic util.CtxKey = "mnemonic"

var errNotTypeMnemonic = errors.New("context value is not of type *mnemonic.MnemonicGenerator")

//go:generate mockery --name Generator --disable-version-string
type Generator interface {
	GetRoomName(context.Context) string
}

type MnemonicGenerator struct {
	arr []string
	m   *mnemonic.Mnemonic
}

func Init() Generator {
	m, _ := mnemonic.NewRandom(256, mnemonic.English)
	s := m.Sentence()
	var arr []string
	arr = append(arr, strings.Split(s, " ")...)
	return &MnemonicGenerator{
		m:   m,
		arr: arr,
	}
}

func (m *MnemonicGenerator) GetRoomName(ctx context.Context) string {
	ctx, span := otel.Tracer(util.Provisioner).Start(ctx, "GetRoomName")
	defer span.End()
	logger, err := util.LoggerFromContext(ctx)
	if err != nil {
		panic(err)
	}
	if len(m.arr) == 0 {
		logger.Ctx(ctx).Info("Generating new sentence")
		m.m, _ = mnemonic.NewRandom(256, mnemonic.English)
		s := m.m.Sentence()
		m.arr = append(m.arr, strings.Split(s, " ")...)
	}

	nemonic := fmt.Sprintf("%s-%s-%s", m.arr[0], m.arr[1], m.arr[2])
	logger.Ctx(ctx).Info("mnemonic generated", zap.String("mnemonic", nemonic))
	m.arr = m.arr[3:]
	return nemonic
}
