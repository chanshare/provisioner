package mnemonic

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/uptrace/opentelemetry-go-extra/otelzap"
	"gitlab.com/chanshare/provisioner/util"
	"go.uber.org/zap"
)

func Test_Init(t *testing.T) {
	tests := []struct {
		name string
		// ctx         context.Context
		shouldPanic bool
	}{
		{
			name: "happy path",
			// ctx:         context.WithValue(context.Background(), "logger", zap.NewExample()),
			shouldPanic: false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			defer func() {
				if r := recover(); r == nil && tt.shouldPanic {
					t.Fail()
				}
			}()
			Init()
		})
	}
}

func Test_GetRoomNameNoLogger(t *testing.T) {
	defer func() {
		if r := recover(); r == nil {
			t.Fail()
		}
	}()
	mnGen := Init()

	ctx := context.Background()
	_ = mnGen.GetRoomName(ctx)
}

func Test_GetRoomNameType(t *testing.T) {
	mnGen := Init()

	ctx := context.WithValue(context.Background(), util.Logger, otelzap.New(zap.NewExample()))
	roomName := mnGen.GetRoomName(ctx)
	assert.IsType(t, "string", roomName)
}

func Test_GetRoomNameDifference(t *testing.T) {
	mnGen := Init()
	var roomNames []string

	for i := 0; i <= 100; i++ {
		ctx := context.WithValue(context.Background(), util.Logger, otelzap.New(zap.NewExample()))
		roomName := mnGen.GetRoomName(ctx)
		assert.NotContains(t, roomNames, roomName)
		roomNames = append(roomNames, roomName)
	}

}
