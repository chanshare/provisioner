package model

type Resp struct {
	RoomName string `json:"room_name"`
	RoomURL  string `json:"room_url"`
}
