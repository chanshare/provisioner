package model

import (
	"errors"

	validation "github.com/go-ozzo/ozzo-validation"
)

/*
"user": "string",
"thread_id": "string",
"board_sn": "string"
*/

var errorNoValue = errors.New("Missing field in request")

type CreateRoomRequest struct {
	User     string `json:"user"`
	ThreadID string `json:"thread_id"`
	BoardSN  string `json:"board_sn"`
}

func (c CreateRoomRequest) Validate() error {
	return validation.ValidateStruct(&c,
		validation.Field(&c.User,
			validation.Required),
		validation.Field(&c.ThreadID,
			validation.Required,
			validation.Length(5, 10)),
		validation.Field(&c.BoardSN,
			validation.Required,
			validation.In("3", "a", "aco", "adv", "an", "b", "bant",
				"biz", "c", "cgl", "ck", "cm", "co", "d", "diy", "e", "f",
				"fa", "fit", "g", "gd", "gif", "h", "hc", "his", "hm", "hr",
				"i", "ic", "int", "jp", "k", "lgbt", "lit", "m", "mlp",
				"mu", "n", "news", "o", "out", "p", "po", "pol", "pw", "qa",
				"qst", "r", "r9k", "s", "s4s", "sci", "soc", "sp", "t", "tg",
				"toy", "trash", "trv", "tv", "u", "v", "vg", "vip", "vm",
				"vmg", "vp", "vr", "vrpg", "vst", "vt", "w", "wg", "wsg", "wsr",
				"x", "xs", "y")),
	)

}

type JoinRoomRequest struct {
	User     string `json:"user"`
	RoomName string `json:"room_name"`
}

func (j JoinRoomRequest) Validate() error {
	return validation.ValidateStruct(&j,
		validation.Field(&j.User,
			validation.Required),
		validation.Field(&j.RoomName,
			validation.Required),
	)
}

type DeleteRoomRequest struct {
	User     string `json:"user"`
	RoomName string `json:"room_name"`
}

func (d DeleteRoomRequest) Validate() error {
	return validation.ValidateStruct(&d,
		validation.Field(&d.RoomName,
			validation.Required),
		validation.Field(&d.User,
			validation.Required))
}
