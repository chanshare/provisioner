package util

import (
	"context"
	"errors"

	"github.com/uptrace/opentelemetry-go-extra/otelzap"
	"go.opentelemetry.io/otel"
	"gorm.io/gorm"
)

const Provisioner = "provisioner"

type CtxKey string

var errNoLogger = errors.New("missing logger")
var errNotTypeLogger = errors.New("context value is not of type *otelzap.Logger")
var errNotTypePostgres = errors.New("context value is not of type *gorm.DB")

const (
	Logger   CtxKey = "logger"
	Postgres CtxKey = "postgres"
	Cfg      CtxKey = "config"
)

func LoggerFromContext(ctx context.Context) (*otelzap.Logger, error) {
	ctx, span := otel.Tracer(Provisioner).Start(ctx, "LoggerFromContext")
	defer span.End()
	loggerInf := ctx.Value(Logger)
	logger, ok := loggerInf.(*otelzap.Logger)
	if !ok {
		return nil, errNotTypeLogger
	}

	return logger, nil
}

func PGFromContext(ctx context.Context) (*gorm.DB, error) {
	ctx, span := otel.Tracer(Provisioner).Start(ctx, "PGFromContext")
	defer span.End()
	loggerInf := ctx.Value(Postgres)
	logger, ok := loggerInf.(*gorm.DB)
	if !ok {
		return nil, errNotTypePostgres
	}

	return logger, nil
}
