package main

import (
	"context"
	"fmt"
	"net/http"
	"os"

	"github.com/uptrace/opentelemetry-go-extra/otelgorm"
	"github.com/uptrace/opentelemetry-go-extra/otelzap"
	"gitlab.com/chanshare/provisioner/config"
	"gitlab.com/chanshare/provisioner/db"
	"gitlab.com/chanshare/provisioner/handlers"
	"gitlab.com/chanshare/provisioner/mnemonic"
	"gitlab.com/chanshare/provisioner/util"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/exporters/jaeger"
	"go.opentelemetry.io/otel/propagation"
	"go.opentelemetry.io/otel/sdk/resource"
	tracesdk "go.opentelemetry.io/otel/sdk/trace"
	semconv "go.opentelemetry.io/otel/semconv/v1.10.0"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"moul.io/zapgorm2"
)

func main() {
	logger := buildLogger()
	ctx := context.Background()

	logger.Ctx(ctx).Info("Built logger")
	ctx = context.WithValue(ctx, util.Logger, logger)

	logger.Ctx(ctx).Info("Init config")
	cfg := config.InitConfig(ctx, "./.conf")
	logger.Ctx(ctx).Debug("Config loaded", zap.Any("cfg", cfg))

	dsn := fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%s sslmode=disable TimeZone=Europe/London",
		cfg.Postgres.Host,
		cfg.Postgres.User,
		cfg.Postgres.Password,
		cfg.Postgres.DBName,
		cfg.Postgres.Port,
	)
	gormLogger := zapgorm2.New(logger.Logger)
	gormLogger.SetAsDefault()
	pg, err := gorm.Open(postgres.Open(dsn), &gorm.Config{Logger: gormLogger})
	if err != nil {
		panic(err)
	}

	if err := pg.Use(otelgorm.NewPlugin()); err != nil {
		panic(err)
	}

	ctx = context.WithValue(ctx, util.Postgres, pg)
	err = pg.AutoMigrate(&db.Room{})
	if err != nil {
		panic(err)
	}

	tp, err := buildTracer(cfg.TracerProvider.URL)
	if err != nil {
		panic(err)
	}
	defer func() {
		if err := tp.Shutdown(context.Background()); err != nil {
			logger.Error("Error shutting down tracer provider: %v", zap.Error(err))
		}
	}()
	tracer := tp.Tracer("util.Provisioner")

	mnGen := mnemonic.Init()
	ctx = context.WithValue(ctx, mnemonic.Mnemonic, &mnGen)

	server, err := handlers.NewServer(cfg, tracer, logger, pg)
	if err != nil {
		panic(err)
	}

	addr := cfg.Service.Host + ":" + cfg.Service.Port
	logger.Ctx(ctx).Info("Listener started", zap.String("addr", addr))
	http.Handle("/", server.Router)
	logger.Ctx(ctx).Fatal("server exited", zap.Error(http.ListenAndServe(addr, nil)))
}

func buildLogger() *otelzap.Logger {
	highPriority := zap.LevelEnablerFunc(func(lvl zapcore.Level) bool {
		return lvl >= zapcore.ErrorLevel
	})
	lowPriority := zap.LevelEnablerFunc(func(lvl zapcore.Level) bool {
		return lvl < zapcore.ErrorLevel
	})

	consoleDebugging := zapcore.Lock(os.Stdout)
	consoleErrors := zapcore.Lock(os.Stderr)

	encoderConf := zap.NewDevelopmentEncoderConfig()
	encoderConf.EncodeLevel = zapcore.CapitalColorLevelEncoder
	encoderConf.TimeKey, encoderConf.EncodeTime = "timestamp", zapcore.ISO8601TimeEncoder
	encoder := zapcore.NewConsoleEncoder(encoderConf)

	cores := zapcore.NewTee(
		zapcore.NewCore(encoder, consoleErrors, highPriority),
		zapcore.NewCore(encoder, consoleDebugging, lowPriority),
	)
	return otelzap.New(zap.New(cores, zap.AddCaller()))
}

func buildTracer(url string) (*tracesdk.TracerProvider, error) {
	exp, err := jaeger.New(jaeger.WithCollectorEndpoint(jaeger.WithEndpoint(url)))
	if err != nil {
		return nil, err
	}

	tp := tracesdk.NewTracerProvider(
		tracesdk.WithBatcher(exp),
		tracesdk.WithResource(resource.NewWithAttributes(
			semconv.SchemaURL,
			semconv.ServiceNameKey.String(util.Provisioner),
			attribute.String("env", "todo"),
		)),
	)

	otel.SetTracerProvider(tp)
	otel.SetTextMapPropagator(propagation.NewCompositeTextMapPropagator(propagation.TraceContext{}, propagation.Baggage{}))

	return tp, nil
}
