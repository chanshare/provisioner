package config

import (
	"context"
	"fmt"
	"os"

	"github.com/spf13/viper"
	"gitlab.com/chanshare/provisioner/util"
)

// Config The root config structure for the provisioner service
type Config struct {
	Service        ServiceCfg       `mapstructure:"service"`
	ContentCfg     ContentSvcConfig `mapstructure:"content"`
	ManagerCfg     ManagerSvcConfig `mapstructure:"manager"`
	Loadbalancer   LoadbalancerCfg  `mapstructure:"loadbalancer"`
	Postgres       PostgresCfg      `mapstructure:"postgres"`
	TracerProvider TracerProvider   `mapstructure:"tracer"`
}

// ServiceCfg Configuration specific to the provisioner service
type ServiceCfg struct {
	Port string `mapstructure:"port"`
	Host string `mapstructure:"host"`
}

// LoadbalancerCfg config to tell the provisioner about the loadbalancer
type LoadbalancerCfg struct {
	Domain   string `mapstructure:"domain"`
	JoinSlug string `mapstructure:"join-slug"`
}

// ContentSvcConfig config to allow the provisioner to make calls to the content service
type ContentSvcConfig struct {
	Port string `mapstructure:"port"`
	Host string `mapstructure:"host"`
}

// ManagerSvcConfig config to allow the provisioner to make calls to the manager service
type ManagerSvcConfig struct {
	Port string `mapstructure:"port"`
	Host string `mapstructure:"host"`
}

// PostgresCfg config to allow the service to access postgres
type PostgresCfg struct {
	Host     string `mapstructure:"host"`
	Port     string `mapstructure:"port"`
	User     string `mapstructure:"user"`
	Password string `mapstructure:"password"`
	DBName   string `mapstructure:"dbname"`
}

type TracerProvider struct {
	URL string `mapstructure:"url"`
}

func setDefaults() {
	viper.SetDefault("port", "8080")
	viper.SetDefault("host", "localhost")
}

func readConfig(relPath string) Config {
	v := viper.New()
	if _, p := os.LookupEnv("COMP_TST"); p {
		v.SetConfigName("comptst") // name of config file (without extension)
	} else {
		v.SetConfigName("conf") // name of config file (without extension)
	}
	v.SetConfigType("toml") // REQUIRED if the config file does not have the extension in the name
	// viper.AddConfigPath("/etc/prov/") // path to look for the config file in
	v.AddConfigPath(relPath) // call multiple times to add many search paths
	if err := v.ReadInConfig(); err != nil {
		panic(fmt.Errorf("failed to read in config %w\n", err))
	}
	var cfg Config
	if err := v.Unmarshal(&cfg); err != nil {
		panic(fmt.Errorf("fatal error config file %w\n", err))
	}
	return cfg
}

// InitConfig initialise the config from a config file
func InitConfig(ctx context.Context, relPath string) Config {
	logger, err := util.LoggerFromContext(ctx)
	if err != nil {
		panic(err)
	}

	logger.Info("Setting config defaults")
	setDefaults()

	logger.Info("Reading config")
	cfg := readConfig(relPath)

	return cfg
}
