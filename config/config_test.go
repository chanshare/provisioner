package config

import (
	"context"
	"testing"

	"github.com/spf13/viper"
	"github.com/stretchr/testify/assert"
	"gitlab.com/chanshare/provisioner/util"
	"go.uber.org/zap"
)

func Test_setDefaults(t *testing.T) {
	setDefaults()

	assert.Equal(t, viper.GetString("port"), "8080")
	assert.Equal(t, viper.GetString("host"), "localhost")
}

func Test_readConfig(t *testing.T) {
	tests := []struct {
		name        string
		shouldPanic bool
		relPath     string
	}{
		{name: "happy path", shouldPanic: false, relPath: "../.conf"},
		{name: "unhappy path no config", shouldPanic: true, relPath: "./.conf"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			defer func() {
				if r := recover(); r != nil && !tt.shouldPanic {
					t.Log("Failing from recover")
					t.Fail()
				}
			}()
			readConfig(tt.relPath)
		})
	}
}

func Test_InitConfig(t *testing.T) {
	tests := []struct {
		Name        string
		Ctx         context.Context
		ShouldPanic bool
		RelPath     string
	}{
		{
			Name:        "happy path",
			Ctx:         genCtx(t),
			ShouldPanic: false,
			RelPath:     "../.conf",
		},
		{
			Name:        "unhappy path no logger",
			Ctx:         context.Background(),
			ShouldPanic: true,
			RelPath:     "../.conf",
		},
		{
			Name:        "unhappy path bad path",
			Ctx:         context.WithValue(context.Background(), util.Logger, zap.NewExample()),
			ShouldPanic: true,
			RelPath:     "/tmp",
		},
	}

	for _, tt := range tests {
		t.Run(tt.Name, func(t *testing.T) {
			defer func() {
				if r := recover(); r == nil && tt.ShouldPanic {
					t.Fail()
				}
			}()

			cfg := InitConfig(tt.Ctx, tt.RelPath)

			t.Log(cfg)

			assert.Equal(t, "8080", cfg.Service.Port)
			assert.Equal(t, "localhost", cfg.Service.Host)
			assert.Equal(t, "http://localhost", cfg.ContentCfg.Host)
			assert.Equal(t, "", cfg.ManagerCfg.Host)
			assert.Equal(t, "chanshare.io", cfg.Loadbalancer.Domain)
			assert.Equal(t, "join", cfg.Loadbalancer.JoinSlug)
		})
	}
}

func genCtx(t *testing.T) context.Context {
	ctx := context.Background()
	ctx = context.WithValue(ctx, util.Logger, zap.NewExample())
	return ctx
}
