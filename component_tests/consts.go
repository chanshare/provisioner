package componenttests

const dummyPlaylistMock = `{"request": {"url": "/playlist"},"response": {"jsonBody": {"playlist": ["media1","media2"]}}}`
const roomCreateMock = `{"request": {"url": "/room"}, "response": {"status": 201}}`

const createRoomPayload = `{"user": "tester123", "thread_id": "1234123", "board_sn": "gif"}`
const joinRoomPayload = `{"user": "tester234", "room_name": "ROOMNAME"}`
