package componenttests

import (
	"bytes"
	"encoding/json"
	"net/http"
	"regexp"
	"testing"

	. "github.com/franela/goblin"
	"gitlab.com/chanshare/provisioner/model"
)

// Component testing method
// 1. Check/populate mocks
// 2. Make Call
// 3. Verify Response

const contentType = "application/json"

var roomName string

func Test(t *testing.T) {
	g := Goblin(t)
	g.Describe("Setup Mocks", func() {
		g.It("Should have mocks set", func() {
			r := bytes.NewReader([]byte(dummyPlaylistMock))
			resp, err := http.Post("http://localhost:7070/__admin/mappings", contentType, r)
			g.Assert(err).IsNil()
			g.Assert(resp.StatusCode).Eql(http.StatusCreated)

			r = bytes.NewReader([]byte(roomCreateMock))
			resp, err = http.Post("http://localhost:7070/__admin/mappings", contentType, r)
			g.Assert(err).IsNil()
			g.Assert(resp.StatusCode).Eql(http.StatusCreated)
		})
	})
	g.Describe("Create Room", func() {
		g.It("Should call the create room endpoint", func() {
			r := bytes.NewReader([]byte(createRoomPayload))
			resp, err := http.Post("http://localhost:8080/create_room", contentType, r)
			g.Assert(err).IsNil()
			g.Assert(resp.StatusCode).Eql(http.StatusCreated)

			var respStruct model.Resp
			err = json.NewDecoder(resp.Body).Decode(&respStruct)
			g.Assert(err).IsNil()
			roomName = respStruct.RoomName

			var roomNameRxp = regexp.MustCompile(`[a-z]+-[a-z]+-[a-z]+`)
			valid := roomNameRxp.MatchString(respStruct.RoomName)
			g.Assert(valid).IsTrue()
		})
	})
	g.Describe("Join Room", func() {
		g.It("Should Join the created room", func() {
			r := bytes.NewReader(
				bytes.Replace(
					[]byte(joinRoomPayload),
					[]byte("ROOMNAME"),
					[]byte(roomName),
					1,
				),
			)

			resp, err := http.Post("http://localhost:8080/join_room", contentType, r)
			g.Assert(err).IsNil()
			g.Assert(resp.StatusCode).Eql(http.StatusOK)

			var respStruct model.Resp
			err = json.NewDecoder(resp.Body).Decode(&respStruct)
			g.Assert(err).IsNil()
		})
	})
	g.Describe("Delete Mocks", func() {
		g.It("Should delete stubs", func() {
			_, err := http.NewRequest(http.MethodDelete, "http://localhost:7070/__admin/mappings", nil)
			g.Assert(err).IsNil()
		})
	})
}
