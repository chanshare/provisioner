package handlers

import (
	"context"
	"reflect"
	"testing"

	sqlmock "github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
	"github.com/uptrace/opentelemetry-go-extra/otelzap"
	"gitlab.com/chanshare/provisioner/clients/cdn"
	"gitlab.com/chanshare/provisioner/config"
	"gitlab.com/chanshare/provisioner/mnemonic"
	"go.opentelemetry.io/otel/sdk/trace"
	"go.uber.org/zap"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

// TODO: This is not a very good test
func TestNewServer(t *testing.T) {
	type args struct {
		ctx  context.Context
		conf config.Config
	}
	tests := []struct {
		name string
		args args
	}{
		{
			name: "HappyPath",
			args: args{
				ctx:  genCtx(t),
				conf: config.InitConfig(genCtx(t), "../.conf"),
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// TODO!
			tp := trace.NewTracerProvider()
			exp := tp.Tracer("test")

			db, _, err := sqlmock.New()
			db.Begin()
			defer db.Close()
			assert.NoError(t, err)
			dialector := postgres.New(postgres.Config{
				DSN:                  "sqlmock_db_0",
				DriverName:           "postgres",
				Conn:                 db,
				PreferSimpleProtocol: true,
			})
			pg, err := gorm.Open(dialector)
			assert.NoError(t, err)

			svr, err := NewServer(tt.args.conf, exp, otelzap.New(zap.NewExample()), pg)
			if err != nil {
				t.FailNow()
			}
			cdnClientInterface := reflect.TypeOf((*cdn.Client)(nil)).Elem()
			if !reflect.TypeOf(svr.CdnClient).Implements(cdnClientInterface) {
				t.FailNow()
			}
			mnGenInterface := reflect.TypeOf((*mnemonic.Generator)(nil)).Elem()
			if !reflect.TypeOf(svr.MnGen).Implements(mnGenInterface) {
				t.FailNow()
			}
		})
	}
}
