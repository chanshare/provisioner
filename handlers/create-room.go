package handlers

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/valyala/fasthttp"
	"gitlab.com/chanshare/provisioner/db"
	"gitlab.com/chanshare/provisioner/model"
	"gitlab.com/chanshare/provisioner/util"
	"go.opentelemetry.io/otel/codes"
	"go.opentelemetry.io/otel/trace"
	"go.uber.org/zap"
)

func (s *Server) CreateRoomHandler(rw http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	span := trace.SpanFromContext(ctx)
	defer span.End()
	logger, err := util.LoggerFromContext(ctx)
	if err != nil {
		writeError(rw, err, http.StatusInternalServerError)
		span.RecordError(err)
		span.SetStatus(codes.Error, err.Error())
		return
	}

	pg, err := util.PGFromContext(ctx)
	if err != nil {
		writeError(rw, err, http.StatusInternalServerError)
		return
	}

	var request model.CreateRoomRequest
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		logger.Ctx(ctx).Error("Failed to decode request", zap.Error(err))
		writeError(rw, err, fasthttp.StatusBadRequest)
		span.RecordError(err)
		span.SetStatus(codes.Error, err.Error())
		return
	}

	if err = request.Validate(); err != nil {
		logger.Ctx(ctx).Warn("Failed to validate request", zap.Error(err))
		writeError(rw, err, fasthttp.StatusBadRequest)
		span.RecordError(err)
		span.SetStatus(codes.Error, err.Error())
		return
	}

	logger.Ctx(ctx).Info("Started CreateRoomHandler")

	// Call content service
	playlist, err := s.CdnClient.GetPlaylist(ctx, request.BoardSN, request.ThreadID)
	if err != nil {
		logger.Ctx(ctx).Error("Failed to get playlist", zap.Error(err))
		writeError(rw, err, fasthttp.StatusInternalServerError)
		span.RecordError(err)
		span.SetStatus(codes.Error, err.Error())
		return
	}

	// Generate Name
	roomName := s.MnGen.GetRoomName(ctx)

	// Build Room struct
	room := db.Room{
		Name:     roomName,
		Playlist: playlist,
		Users:    []string{request.User},
	}
	logger.Ctx(ctx).Info("Created room struct", zap.Any("room", room))

	// Place room into posgres
	result := pg.WithContext(ctx).Create(&room)
	if result.Error != nil {
		logger.Ctx(ctx).Error("Failed to insert room into pg", zap.Error(err))
		writeError(rw, result.Error, fasthttp.StatusInternalServerError)
		span.RecordError(result.Error)
		span.SetStatus(codes.Error, result.Error.Error())
		return
	}
	// Send new room to room manager
	err = s.ManagerClient.CreateRoom(ctx, request.User, playlist, roomName)
	if err != nil {
		writeError(rw, err, fasthttp.StatusInternalServerError)
		span.RecordError(err)
		span.SetStatus(codes.Error, err.Error())
		return
	}

	rw.WriteHeader(http.StatusCreated)
	// Send new room to client
	resp := model.Resp{
		RoomName: roomName,
		RoomURL:  fmt.Sprintf("%s.%s/%s", s.Config.Loadbalancer.JoinSlug, s.Config.Loadbalancer.Domain, roomName),
	}
	if err := json.NewEncoder(rw).Encode(resp); err != nil {
		logger.Ctx(ctx).Error("Failed to encode response struct")
		writeError(rw, err, fasthttp.StatusInternalServerError)
		span.RecordError(err)
		span.SetStatus(codes.Error, err.Error())
		return
	}
	logger.Ctx(ctx).Info("Room created")
}
