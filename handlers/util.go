package handlers

import (
	"encoding/json"
	"errors"
	"net/http"
)

type ErrResp struct {
	Err string `json:"error"`
}

var noErrErr = errors.New("there is no error")

func writeError(rw http.ResponseWriter, errToSend error, status int) {
	e := ErrResp{
		Err: errToSend.Error(),
	}

	rw.WriteHeader(status)
	_ = json.NewEncoder(rw).Encode(&e)
}
