package handlers

//
// import (
// 	"errors"
// 	"fmt"
// 	"io"
// 	"net/http"
// 	"net/http/httptest"
// 	"testing"
//
// 	"github.com/stretchr/testify/assert"
// )
//
// var testingError = errors.New("testing error")
//
// // Testing line 25 is hard
// func Test_writeError(t *testing.T) {
// 	tests := []struct {
// 		Name      string
// 		Err       error
// 		Status    int
// 		ShouldErr bool
//                 ErrType error
// 	}{
// 		{
// 			Name:      "HappyPath",
// 			Err:       testingError,
// 			Status:    http.StatusTeapot,
// 			ShouldErr: false,
// 		},
// 		{
// 			Name:      "UnhappyPath",
// 			Err:       nil,
// 			Status:    http.StatusInternalServerError,
// 			ShouldErr: true,
//                         ErrType: noErrErr,
// 		},
// 	}
//
// 	for _, tt := range tests {
// 		t.Run(tt.Name, func(t *testing.T) {
// 			ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
// 				err := writeError(w, tt.Err, tt.Status)
// 				if tt.ShouldErr {
// 					assert.Error(t, err)
//                                         assert.Equal(t, tt.ErrType, err)
//                                         return
// 				} else {
// 					assert.NoError(t, err)
//                                         return
// 				}
// 			}))
// 			defer ts.Close()
// 			if !tt.ShouldErr {
// 				res, err := http.Get(ts.URL)
// 				assert.NoError(t, err)
//
// 				assert.Equal(t, tt.Status, res.StatusCode)
// 				bodyBytes, err := io.ReadAll(res.Body)
// 				assert.NoError(t, err)
// 				assert.Equal(t, fmt.Sprintf(`{"error":"%s"}`, tt.Err.Error()), string(bodyBytes))
// 			}
// 		})
// 	}
// }
