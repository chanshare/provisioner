package handlers

import (
	"encoding/json"
	"net/http"

	"github.com/samber/lo"
	"github.com/valyala/fasthttp"
	"gitlab.com/chanshare/provisioner/db"
	"gitlab.com/chanshare/provisioner/model"
	"gitlab.com/chanshare/provisioner/util"
	"go.opentelemetry.io/otel/codes"
	"go.opentelemetry.io/otel/trace"
	"go.uber.org/zap"
)

func (s *Server) DeleteRoomHandler(rw http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	span := trace.SpanFromContext(ctx)
	defer span.End()
	logger, err := util.LoggerFromContext(ctx)
	if err != nil {
		writeError(rw, err, fasthttp.StatusInternalServerError)
		span.RecordError(err)
		span.SetStatus(codes.Error, err.Error())
		return
	}
	pg, err := util.PGFromContext(ctx)
	if err != nil {
		writeError(rw, err, fasthttp.StatusInternalServerError)
		span.RecordError(err)
		span.SetStatus(codes.Error, err.Error())
		return
	}

	var request model.DeleteRoomRequest
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		logger.Ctx(ctx).Error("Failed to decode request")
		writeError(rw, err, fasthttp.StatusBadRequest)
		span.RecordError(err)
		span.SetStatus(codes.Error, err.Error())
		return
	}
	logger.Ctx(ctx).Info("Unmarshaled request")

	if err := request.Validate(); err != nil {
		logger.Ctx(ctx).Warn("Validation of delete request failed")
		writeError(rw, err, fasthttp.StatusBadRequest)
		span.RecordError(err)
		span.SetStatus(codes.Error, err.Error())
		return
	}

	var room db.Room
	tx := pg.WithContext(ctx).Debug().First(&room, "name = ?", request.RoomName)
	if tx.Error != nil {
		logger.Ctx(ctx).Error("Could not find room in db")
		writeError(rw, tx.Error, fasthttp.StatusInternalServerError)
		span.RecordError(tx.Error)
		span.SetStatus(codes.Error, tx.Error.Error())
		return
	}
	logger.Ctx(ctx).Info("Got room", zap.Any("room", room))
	logger.Ctx(ctx).Info("Got users", zap.Any("users", room.Users))

	if lo.Contains(room.Users, request.User) || request.User == "SYSTEM" {
		logger.Ctx(ctx).Info("Deleting room")
		tx := pg.WithContext(ctx).Debug().Delete(&room)
		if tx.Error != nil {
			logger.Ctx(ctx).Error("Failed to delete room")
			writeError(rw, tx.Error, http.StatusInternalServerError)
			span.RecordError(tx.Error)
			span.SetStatus(codes.Error, tx.Error.Error())
			return
		}
	}

	rw.WriteHeader(http.StatusOK)
}
