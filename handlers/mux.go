package handlers

import (
	"github.com/uptrace/opentelemetry-go-extra/otelzap"
	"gitlab.com/chanshare/provisioner/config"
	"gitlab.com/chanshare/provisioner/mnemonic"
	"gitlab.com/chanshare/provisioner/util"
	"go.opentelemetry.io/contrib/instrumentation/github.com/gorilla/mux/otelmux"
	"go.opentelemetry.io/otel/trace"
	"gorm.io/gorm"

	"github.com/gorilla/mux"
	"gitlab.com/chanshare/provisioner/clients/cdn"
	"gitlab.com/chanshare/provisioner/clients/manager"
)

const (
	CreateRoom = "/create_room"
	JoinRoom   = "/join_room"
	DeleteRoom = "/delete_room"
)

type Server struct {
	CdnClient     cdn.Client
	ManagerClient manager.Client
	MnGen         mnemonic.Generator
	Router        *mux.Router
	Tracer        trace.Tracer
	Config        config.Config
}

func NewServer(conf config.Config, tracer trace.Tracer, logger *otelzap.Logger, pg *gorm.DB) (Server, error) {
	r := mux.NewRouter()
	s := Server{
		CdnClient:     cdn.NewCdnClient(conf.ContentCfg.Host, conf.ContentCfg.Port),
		ManagerClient: manager.NewManagerClient(conf.ManagerCfg.Host, conf.ManagerCfg.Port),
		MnGen:         mnemonic.Init(),
		Router:        r,
		Tracer:        tracer,
		Config:        conf,
	}
	r.Use(middleware(logger, pg))
	r.Use(otelmux.Middleware(util.Provisioner))
	r.HandleFunc(CreateRoom, s.CreateRoomHandler).Methods("POST")
	r.HandleFunc(JoinRoom, s.JoinRoomHandler).Methods("POST")
	r.HandleFunc(DeleteRoom, s.DeleteRoomHandler).Methods("POST")
	return s, nil
}
