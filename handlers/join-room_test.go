package handlers

import (
	"context"
	"net/http"
	"net/http/httptest"
	"regexp"
	"strings"
	"testing"

	sqlmock "github.com/DATA-DOG/go-sqlmock"
	"github.com/lib/pq"
	"github.com/stretchr/testify/assert"
	cdnMock "gitlab.com/chanshare/provisioner/clients/cdn/mocks"
	"gitlab.com/chanshare/provisioner/config"
	mnGenMock "gitlab.com/chanshare/provisioner/mnemonic/mocks"
	"gitlab.com/chanshare/provisioner/util"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

func Test_JoinRoomHandler(t *testing.T) {
	tests := []struct {
		Name         string
		Req          string
		Ctx          context.Context
		StatusCode   int
		PGInCtx      bool
		Expectations func(s sqlmock.Sqlmock)
	}{{
		Name:       "HappyPath",
		Req:        joinHappyPath,
		Ctx:        genCtx(t),
		StatusCode: http.StatusOK,
		PGInCtx:    true,
		Expectations: func(s sqlmock.Sqlmock) {
			rows := sqlmock.NewRows([]string{"name", "users", "playlist"}).AddRow(
				"test-room",
				pq.StringArray([]string{"string"}),
				pq.StringArray([]string{"media1", "media2"}))
			s.ExpectQuery(regexp.QuoteMeta(`SELECT * FROM "rooms" WHERE name = $1 ORDER BY "rooms"."name" LIMIT 1`)).
				WithArgs("test-room").
				WillReturnRows(rows)
		},
	},
		{
			Name:       "UnhappyPath malformed request",
			Req:        joinUnhappyPath,
			Ctx:        genCtx(t),
			StatusCode: http.StatusBadRequest,
			PGInCtx:    true,
			Expectations: func(s sqlmock.Sqlmock) {
			},
		},
		{
			Name:       "UnhappyPath missing body fields",
			Req:        joinUnhappyPathVal,
			Ctx:        genCtx(t),
			StatusCode: http.StatusBadRequest,
			PGInCtx:    true,
			Expectations: func(s sqlmock.Sqlmock) {
			},
		},
		{
			Name:       "UnhappyPath missing logger from ctx",
			Req:        joinUnhappyPathVal,
			Ctx:        context.Background(),
			StatusCode: http.StatusInternalServerError,
			PGInCtx:    false,
			Expectations: func(s sqlmock.Sqlmock) {
			},
		},
		{
			Name:       "UnhappyPath missing pg from ctx",
			Req:        joinUnhappyPathVal,
			Ctx:        genCtx(t),
			StatusCode: http.StatusInternalServerError,
			PGInCtx:    false,
			Expectations: func(s sqlmock.Sqlmock) {
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.Name, func(t *testing.T) {
			db, mocksql, err := sqlmock.New()
			db.Begin()
			defer db.Close()
			assert.NoError(t, err)

			// Needed for the server struct
			cdnClientMock := cdnMock.NewClient(t)
			mnemonicMock := mnGenMock.NewGenerator(t)

			tt.Expectations(mocksql)

			dialector := postgres.New(postgres.Config{
				DSN:                  "sqlmock_db_0",
				DriverName:           "postgres",
				Conn:                 db,
				PreferSimpleProtocol: true,
			})
			pg, err := gorm.Open(dialector)
			assert.NoError(t, err)
			if tt.PGInCtx {
				tt.Ctx = context.WithValue(tt.Ctx, util.Postgres, pg)
			}

			server := Server{
				CdnClient: cdnClientMock,
				MnGen:     mnemonicMock,
				Config:    config.InitConfig(genCtx(t), "../.conf"),
			}

			req := httptest.NewRequest("POST", "/create_room", strings.NewReader(tt.Req))
			w := httptest.NewRecorder()
			req = req.WithContext(tt.Ctx)
			server.JoinRoomHandler(w, req)

			assert.Equal(t, tt.StatusCode, w.Result().StatusCode)
			assert.NoError(t, mocksql.ExpectationsWereMet())
		})
	}
}

const joinHappyPath = `{
	"user": "string",
	"room_name": "test-room"
	}`

const joinUnhappyPath = `
	"user": "string"
	"room_name": "test-room"
	}`

const joinUnhappyPathVal = `{
	"room_name": "test-room"
	}`
