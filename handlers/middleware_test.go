package handlers

import (
	"context"
	"net/http"
	"net/http/httptest"
	"reflect"
	"testing"

	sqlmock "github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
	"github.com/uptrace/opentelemetry-go-extra/otelzap"
	"gitlab.com/chanshare/provisioner/util"
	"go.uber.org/zap"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

func Test_middleware(t *testing.T) {
	db, _, err := sqlmock.New()
	db.Begin()
	defer db.Close()
	assert.NoError(t, err)
	dialector := postgres.New(postgres.Config{
		DSN:                  "sqlmock_db_0",
		DriverName:           "postgres",
		Conn:                 db,
		PreferSimpleProtocol: true,
	})
	pg, err := gorm.Open(dialector)
	assert.NoError(t, err)

	type args struct {
		logger *otelzap.Logger
		pg     *gorm.DB
	}
	tests := []struct {
		name string
		args args
		want context.Context
	}{
		{
			name: "HappyPath",
			args: args{
				logger: otelzap.New(zap.NewExample()),
				pg:     pg,
			},
			want: context.WithValue(genCtx(t), util.Postgres, pg),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := middleware(tt.args.logger, tt.args.pg)

			handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				ctx := r.Context()
				if reflect.TypeOf(ctx.Value(util.Logger)).String() != "*otelzap.Logger" {
					t.FailNow()
				}
				if reflect.TypeOf(ctx.Value(util.Postgres)).String() != "*gorm.DB" {
					t.FailNow()
				}
			})
			req := httptest.NewRequest("GET", "http://testing", nil)

			testHandler := got(handler)

			// call the handler using a mock response recorder (we'll not use that anyway)
			testHandler.ServeHTTP(httptest.NewRecorder(), req)
		})
	}
}
