package handlers

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/valyala/fasthttp"
	"gitlab.com/chanshare/provisioner/db"
	"gitlab.com/chanshare/provisioner/model"
	"gitlab.com/chanshare/provisioner/util"
	"go.opentelemetry.io/otel/codes"
	"go.opentelemetry.io/otel/trace"
	"go.uber.org/zap"
)

func (s *Server) JoinRoomHandler(rw http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	span := trace.SpanFromContext(ctx)
	defer span.End()
	logger, err := util.LoggerFromContext(ctx)
	if err != nil {
		writeError(rw, err, fasthttp.StatusInternalServerError)
		span.RecordError(err)
		span.SetStatus(codes.Error, err.Error())
		return
	}
	pg, err := util.PGFromContext(ctx)
	if err != nil {
		writeError(rw, err, fasthttp.StatusInternalServerError)
		span.RecordError(err)
		span.SetStatus(codes.Error, err.Error())
		return
	}
	var request model.JoinRoomRequest
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		logger.Ctx(ctx).Error("Failed to decode request", zap.Error(err))
		writeError(rw, err, fasthttp.StatusBadRequest)
		span.RecordError(err)
		span.SetStatus(codes.Error, err.Error())
		return
	}

	if err := request.Validate(); err != nil {
		logger.Ctx(ctx).Warn("Failed to validate request", zap.Error(err))
		writeError(rw, err, fasthttp.StatusBadRequest)
		span.RecordError(err)
		span.SetStatus(codes.Error, err.Error())
		return
	}

	// Call pg to get room details
	var room db.Room
	pg.WithContext(ctx).First(&room, "name = ?", request.RoomName)

	// Save added user back to db
	pg.WithContext(ctx).Model(&room).Update("users", append(room.Users, request.User))

	resp := model.Resp{
		RoomName: request.RoomName,
		RoomURL: fmt.Sprintf(
			"%s.%s/%s",
			s.Config.Loadbalancer.JoinSlug,
			s.Config.Loadbalancer.Domain,
			request.RoomName,
		),
	}
	if err := json.NewEncoder(rw).Encode(resp); err != nil {
		logger.Ctx(ctx).Error("Failed to encode response struct", zap.Error(err))
		writeError(rw, err, fasthttp.StatusInternalServerError)
		span.RecordError(err)
		span.SetStatus(codes.Error, err.Error())
		return
	}
	logger.Ctx(ctx).Info("Room joined", zap.String("room-name", request.RoomName), zap.String("user", request.User))
}
