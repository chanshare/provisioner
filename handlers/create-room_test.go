package handlers

import (
	"context"
	"errors"
	"net/http"
	"net/http/httptest"
	"regexp"
	"strings"
	"testing"

	"github.com/lib/pq"
	"github.com/uptrace/opentelemetry-go-extra/otelzap"

	"gitlab.com/chanshare/provisioner/config"
	mnGenMock "gitlab.com/chanshare/provisioner/mnemonic/mocks"

	sqlmock "github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	cdnMock "gitlab.com/chanshare/provisioner/clients/cdn/mocks"
	managerMock "gitlab.com/chanshare/provisioner/clients/manager/mocks"
	"gitlab.com/chanshare/provisioner/util"
	"go.opentelemetry.io/contrib/instrumentation/github.com/gorilla/mux/otelmux"
	"go.uber.org/zap"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

func Test_CreateRoomHandler(t *testing.T) {
	tests := []struct {
		Name         string
		Ctx          context.Context
		Req          string
		StatusCode   int
		PGInCtx      bool
		Expectations func(s sqlmock.Sqlmock, cdnClientMock *cdnMock.Client, mnemonicMock *mnGenMock.Generator, managerClientMock *managerMock.Client)
	}{
		{
			Name:       "Happy path",
			Ctx:        genCtx(t),
			Req:        happyPathJson,
			StatusCode: http.StatusCreated,
			PGInCtx:    true,
			Expectations: func(s sqlmock.Sqlmock, cdnClientMock *cdnMock.Client, mnemonicMock *mnGenMock.Generator, managerClientMock *managerMock.Client) {
				s.MatchExpectationsInOrder(false)
				s.ExpectBegin()
				s.ExpectExec(regexp.QuoteMeta(`INSERT INTO "rooms" ("name","playlist","users") VALUES ($1,$2,$3)`)).
					WithArgs("test-room", pq.StringArray{"media1", "media2"}, pq.StringArray{"tester"}).
					WillReturnResult(sqlmock.NewResult(1, 1))
				s.ExpectCommit()
				cdnClientMock.On("GetPlaylist", mock.Anything, mock.Anything, mock.Anything).
					Return([]string{"media1", "media2"}, nil)
				managerClientMock.On("CreateRoom", mock.Anything, "tester", []string{"media1", "media2"}, "test-room").
					Return(nil)
				mnemonicMock.On("GetRoomName", mock.Anything).Return("test-room")
			},
		},
		{
			Name:       "Unhappy path, invalid request",
			Req:        unhappyPathJson,
			Ctx:        genCtx(t),
			StatusCode: http.StatusBadRequest,
			PGInCtx:    true,
			Expectations: func(s sqlmock.Sqlmock, cdnClientMock *cdnMock.Client, mnemonicMock *mnGenMock.Generator, managerClientMock *managerMock.Client) {
			},
		},
		{
			Name:       "Unhappy path, no logger",
			Req:        happyPathJson,
			Ctx:        context.Background(),
			StatusCode: http.StatusInternalServerError,
			PGInCtx:    true,
			Expectations: func(s sqlmock.Sqlmock, cdnClientMock *cdnMock.Client, mnemonicMock *mnGenMock.Generator, managerClientMock *managerMock.Client) {
			},
		},
		{
			Name:       "Unhappy path, no pg",
			Req:        happyPathJson,
			Ctx:        genCtx(t),
			StatusCode: http.StatusInternalServerError,
			PGInCtx:    false,
			Expectations: func(s sqlmock.Sqlmock, cdnClientMock *cdnMock.Client, mnemonicMock *mnGenMock.Generator, managerClientMock *managerMock.Client) {
			},
		},
		{
			Name:       "Unappy path cdn errors",
			Ctx:        genCtx(t),
			Req:        happyPathJson,
			StatusCode: http.StatusInternalServerError,
			PGInCtx:    true,
			Expectations: func(s sqlmock.Sqlmock, cdnClientMock *cdnMock.Client, mnemonicMock *mnGenMock.Generator, managerClientMock *managerMock.Client) {
				cdnClientMock.On("GetPlaylist", mock.Anything, mock.Anything, mock.Anything).
					Return([]string{""}, errors.New("testing error"))
			},
		},
		{
			Name:       "unappy path bad pg",
			Ctx:        genCtx(t),
			Req:        happyPathJson,
			StatusCode: http.StatusInternalServerError,
			PGInCtx:    true,
			Expectations: func(s sqlmock.Sqlmock, cdnClientMock *cdnMock.Client, mnemonicMock *mnGenMock.Generator, managerClientMock *managerMock.Client) {
				s.MatchExpectationsInOrder(false)
				s.ExpectBegin()
				s.ExpectExec(regexp.QuoteMeta(`INSERT INTO "rooms" ("name","playlist","users") VALUES ($1,$2,$3)`)).
					WithArgs("test-room", pq.StringArray{"media1", "media2"}, pq.StringArray{"tester"}).
					WillReturnError(errors.New("testing error"))
				s.ExpectRollback()
				cdnClientMock.On("GetPlaylist", mock.Anything, mock.Anything, mock.Anything).
					Return([]string{"media1", "media2"}, nil)
				mnemonicMock.On("GetRoomName", mock.Anything).Return("test-room")
			},
		},
		{
			Name:       "Unhappy path, bad request format",
			Req:        garbledReq,
			Ctx:        genCtx(t),
			StatusCode: http.StatusBadRequest,
			PGInCtx:    true,
			Expectations: func(s sqlmock.Sqlmock, cdnClientMock *cdnMock.Client, mnemonicMock *mnGenMock.Generator, managerClientMock *managerMock.Client) {
			},
		},
		{
			Name:       "Unhappy Path, manager returns error",
			Req:        happyPathJson,
			Ctx:        genCtx(t),
			StatusCode: http.StatusInternalServerError,
			PGInCtx:    true,
			Expectations: func(s sqlmock.Sqlmock, cdnClientMock *cdnMock.Client, mnemonicMock *mnGenMock.Generator, managerClientMock *managerMock.Client) {
				s.MatchExpectationsInOrder(false)
				s.ExpectBegin()
				s.ExpectExec(regexp.QuoteMeta(`INSERT INTO "rooms" ("name","playlist","users") VALUES ($1,$2,$3)`)).
					WithArgs("test-room", pq.StringArray{"media1", "media2"}, pq.StringArray{"tester"}).
					WillReturnResult(sqlmock.NewResult(1, 1))
				s.ExpectCommit()
				cdnClientMock.On("GetPlaylist", mock.Anything, mock.Anything, mock.Anything).
					Return([]string{"media1", "media2"}, nil)
				managerClientMock.On("CreateRoom", mock.Anything, "tester", []string{"media1", "media2"}, "test-room").
					Return(errors.New("manager failed"))
				mnemonicMock.On("GetRoomName", mock.Anything).Return("test-room")
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.Name, func(t *testing.T) {
			db, mocksql, err := sqlmock.New()
			db.Begin()
			defer db.Close()
			assert.NoError(t, err)
			cdnClientMock := cdnMock.NewClient(t)
			managerClientMock := managerMock.NewClient(t)
			mnemonicMock := mnGenMock.NewGenerator(t)
			tt.Expectations(mocksql, cdnClientMock, mnemonicMock, managerClientMock)

			dialector := postgres.New(postgres.Config{
				DSN:                  "sqlmock_db_0",
				DriverName:           "postgres",
				Conn:                 db,
				PreferSimpleProtocol: true,
			})
			pg, err := gorm.Open(dialector)
			assert.NoError(t, err)
			if tt.PGInCtx {
				tt.Ctx = context.WithValue(tt.Ctx, util.Postgres, pg)
			}

			server := Server{
				CdnClient:     cdnClientMock,
				ManagerClient: managerClientMock,
				MnGen:         mnemonicMock,
				Config:        config.InitConfig(genCtx(t), "../.conf"),
			}

			req := httptest.NewRequest("POST", "/create_room", strings.NewReader(tt.Req))
			w := httptest.NewRecorder()
			req = req.WithContext(tt.Ctx)

			mid := otelmux.Middleware("test")

			handler := mid(http.HandlerFunc(server.CreateRoomHandler))

			handler.ServeHTTP(w, req)

			assert.Equal(t, tt.StatusCode, w.Result().StatusCode)
			assert.NoError(t, mocksql.ExpectationsWereMet())
		})
	}
}

func genCtx(t *testing.T) context.Context {
	ctx := context.Background()
	ctx = context.WithValue(ctx, util.Logger, otelzap.New(zap.NewExample()))
	return ctx
}

const happyPathJson = `
	{
"user": "tester",
"thread_id": "string",
"board_sn": "gif"
}
	`

const unhappyPathJson = `
	{
"user": "tester",
"board_sn": "string"
}
	`

const garbledReq = `
	ksldjfghlshdvkajshbldbhl
	dkfjghallksjghruihakljvh
	laskurghuaibyughalzuahbl
	`
