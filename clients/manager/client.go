package manager

import (
	"bytes"
	"context"
	"encoding/json"
	"io/ioutil"
	"net/http"

	"gitlab.com/chanshare/provisioner/util"
	"go.uber.org/zap"
)

const playlistEndpoint = "/room"

type client struct {
	baseUrl string
}

//go:generate mockery --name Client --disable-version-string
type Client interface {
	CreateRoom(context.Context, string, []string, string) error
}

type CreateRoomRequest struct {
	User     string   `json:"user"`
	Playlist []string `json:"playlist"`
	Name     string   `json:"name"`
}

func NewManagerClient(host, port string) Client {
	return &client{
		baseUrl: host + ":" + port,
	}
}

func (c *client) CreateRoom(ctx context.Context, user string, playlist []string, name string) error {
	logger, err := util.LoggerFromContext(ctx)
	if err != nil {
		return err
	}
	req := CreateRoomRequest{
		User:     user,
		Playlist: playlist,
		Name:     name,
	}
	b, err := json.Marshal(&req)
	if err != nil {
		logger.Ctx(ctx).Error("Failed to marshal request")
		return err
	}
	bReader := bytes.NewReader(b)
	resp, err := http.Post(c.baseUrl+playlistEndpoint, "application/json", bReader)
	if err != nil {
		logger.Ctx(ctx).Error("request to start room failed", zap.Error(err))
		return err
	}

	if resp.StatusCode != http.StatusCreated {
		b, _ := ioutil.ReadAll(resp.Body)
		logger.Ctx(ctx).Error("room not created", zap.Any("response body", b))
		return err
	}

	return nil
}
