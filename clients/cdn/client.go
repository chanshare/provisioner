package cdn

import (
	"bytes"
	"context"
	"encoding/json"
	"io"
	"net/http"

	"gitlab.com/chanshare/provisioner/util"
	"go.opentelemetry.io/otel"
	"go.uber.org/zap"
)

const playlistEndpoint = "/playlist"
const contentType = "application/json"

type client struct {
	baseUrl string
}

//go:generate mockery --name Client --disable-version-string
type Client interface {
	GetPlaylist(context.Context, string, string) ([]string, error)
}

type Request struct {
	BoardSn  string `json:"board_sn"`
	ThreadID string `json:"thread_id"`
}

func NewCdnClient(host, port string) Client {
	return &client{
		baseUrl: host + ":" + port,
	}
}

type Response struct {
	Playlist []string `json:"playlist"`
}

func (c client) GetPlaylist(ctx context.Context, boardSn, threadID string) ([]string, error) {
	ctx, span := otel.Tracer(util.Provisioner).Start(ctx, "GetPlaylist")
	defer span.End()
	logger, err := util.LoggerFromContext(ctx)
	if err != nil {
		return []string{""}, err
	}
	req := Request{
		BoardSn:  boardSn,
		ThreadID: threadID,
	}
	var buffer bytes.Buffer
	if err := json.NewEncoder(&buffer).Encode(req); err != nil {
		logger.Error("Failed to encode request", zap.Error(err))
		return []string{""}, err
	}
	resp, err := http.Post(c.baseUrl+playlistEndpoint, contentType, &buffer)
	logger.Info("Encoded content request")
	if err != nil {
		logger.Error("Error posting to content", zap.Error(err))
		return []string{""}, err
	}
	logger.Info("Got response", zap.Any("resp", resp.Body))

	defer resp.Body.Close()

	bytes, err := io.ReadAll(resp.Body)
	if err != nil {
		return []string{}, err
	}
	var respStruct Response
	json.Unmarshal(bytes, &respStruct)
	logger.Info("Returning playlist", zap.Any("playlist", respStruct.Playlist))
	return respStruct.Playlist, nil
}
