package db

import "github.com/lib/pq"

type Room struct {
	Name     string         `gorm:"primaryKey"`
	Playlist pq.StringArray `gorm:"type:text[]"`
	Users    pq.StringArray `gorm:"type:text[]"`
}
